
# coding: utf-8

# # Forex Prediction
# 1. seq2seq: Given a history, predict the coming candle sticks
# 2. RNN with LSTM: Given a history, predict the best action for the next X sticks
#     1. buy, sell, hold
#     2. number of ticks to expect the max
#     3. variation of price in pips
#     4. probability of expectation

# ## Load Data

# In[1]:

import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import StratifiedShuffleSplit
import time
import tensorflow as tf


# In[2]:

filename = "data.csv"
data = pd.read_table(filename,)


# In[3]:

data["tm.day_of_week"].max()


# In[4]:

data.columns


# In[29]:

def get_max_profit(data, idx, nbr_sticks_for_max_profit=30):
    # return number of ticks, variation in pips
    min_pips = 0.0003
    number_ticks = 0
    max_pips = 0.
    to_analyze = data[idx+1:idx+1+nbr_sticks_for_max_profit]
    current_pip = data.get_value(index=idx,col="open")
    for index,row in to_analyze.iterrows():
        i_pips = row['open'] -  current_pip
        if(abs(i_pips) > abs(max_pips)):
            number_ticks = index + 1
            max_pips = i_pips
    # No need to normalize the max_pips since it is already around 0
    # We divide the normalized ticks by 1000 so that its values is getting closser to the normalized value of pips
#     number_ticks = number_ticks / nbr_sticks_for_max_profit / 1000
#     ret = [number_ticks, max_pips]
    ret = [0,0,0]
    if(abs(min_pips)<abs(max_pips)):
        if(max_pips<0):
            ret[1]=1
        else:
            ret[2]=1
    else:
        ret[0]=1
    return ret
#Test
print(get_max_profit(data,0,10))


# In[30]:

def normalize(x,current):
    return float(x)/float(current) - 1;


# In[31]:

def get_history(data,idx, size_history=10):
    #use the current stick as the ref for normalizing
    sub_data = data[idx - size_history:idx]
    #despite the slicing, the index is still kept
    open_ref = sub_data.get_value(index=idx-1,col="open")
    
    # do not normalize the volume according to the max of the window nor to the max
    # of all data. If max of all data, then how is the behaviour of the RNN regarding that max
    # the same for max of window ==> Should the RNN treat a volume of 10/100 the same as 1/10?
    # no!
    
    # open, high, low, close, tick_volume, 12 for month, 31 for day, 24 for hour, 7 for day of the week
    ret = np.zeros((size_history,79))
    i=0
    for idx,row in sub_data.iterrows():
        # open
        ret[i][0]=normalize(row["open"],open_ref)
        # high
        ret[i][1]=normalize(row["high"],open_ref)
        # low
        ret[i][2]=normalize(row["low"],open_ref)
        # close
        ret[i][3]=normalize(row["open"],open_ref)
        # tick_volume
        ret[i][4]=row["tick_volume"]/1000
        # month 1 - 12 ==> 0 - 11
        ret[i][5+int(row["tm.mon"])-1]=1
        # day 1 - 31 ==> 0 - 30
        ret[i][5+12+int(row["tm.day"])-1]=1
        # hour 0 - 23 check data["tm.hour"].max()
        ret[i][5+12+31+int(row["tm.hour"])]=1
        # day of week 1 - 7 (even if no trade on 6-7 or saturday and sunday==> reserve in case of)
        ret[i][5+12+31+24+int(row["tm.day_of_week"])-1]=1
        i+=1
    return ret
test = get_history(data,105,100)
print(test.shape)
print(test)


# In[32]:

def get_XY(test_idx_idx,data,size_history=50,nbr_sticks_for_max_profit=30):
    return np.array([get_history(data,i,size_history) for i in test_idx_idx]),         np.array([get_max_profit(data,i,nbr_sticks_for_max_profit) for i in test_idx_idx])


# In[33]:

# Split Training/Validation and Test
size_history=30
nbr_sticks_for_max_profit=10
# all data is a set of size_history stick and max_profit from next nbr_sticks_for_max_profit
data_idx = list(range(0,len(data)))
data_idx = data_idx[size_history:-nbr_sticks_for_max_profit]
np.random.shuffle(data_idx)
#in case of memory error, reduce the test size
size_train = int(len(data_idx)*0.90)
train_idx_idx, test_idx_idx = data_idx[:size_train], data_idx[size_train:]
size_test = int(len(test_idx_idx))
val_idx_idx, test_idx_idx = test_idx_idx[:size_test//2], test_idx_idx[size_test//2:]
# start = time.clock()
# val_x, val_y = get_XY(val_idx_idx,data,size_history,nbr_sticks_for_max_profit)
# test_x, test_y = get_XY(val_idx_idx,data,size_history,nbr_sticks_for_max_profit)
# print("validation and test data prepared in {}s".format(time.clock()-start))


# In[34]:

# left with extraction of test data
# from Transfer Learning Udacity dlnd
def get_batches(data,train_idx_idx, batch_size=100, size_history=30, nbr_sticks_for_max_profit=30):
    """ Return a generator that yields batches from arrays x and y. """
    n_batches = len(train_idx_idx)//batch_size
    
    # should be full batch only (by experience, passing final state to next batch is giving an error on the model
    # when the batch size is different)
    for ii in range(0, n_batches*batch_size, batch_size):
        train_idx = train_idx_idx[ii: ii+batch_size]
        yield get_XY(train_idx,data,size_history,nbr_sticks_for_max_profit)
# start = time.clock()
# for x,y in get_batches(data,test_idx_idx, 100,30,30):
#     print("{} Batch ready after {}s".format(y,time.clock()-start))
#     start = time.clock()


# In[35]:

# tmax = 0
# ti = 0
# for tx,ty in get_batches(data,data_idx, 300,size_history,nbr_sticks_for_max_profit):
#     for val in ty:
#         ti += 1
#         tcmax = val[0]
#         if(abs(tcmax) > abs(tmax)):
#             tmax = tcmax
#             print("max {} on {}".format(tmax,ti))
# print(tmax)


# # Modeling the RNN
# Name properly so that:
# * weight can be reloaded
# * graph can be visualized with TensorBoard
# * hyper-parameters can be optimized 

# ## Build the graph
# 
# Here, we'll build the graph. First up, defining the hyperparameters.
# 
# * `lstm_size`: Number of units in the hidden layers in the LSTM cells. Usually larger is better performance wise. Common values are 128, 256, 512, etc.
# * `lstm_layers`: Number of LSTM layers in the network. I'd start with 1, then add more if I'm underfitting.
# * `batch_size`: The number of reviews to feed the network in one training pass. Typically this should be set as high as you can go without running out of memory.
# * `learning_rate`: Learning rate

# In[49]:

#the size of the lstm is defining the memory of the RNN
lstm_size = 128
lstm_layers = 2
batch_size = 32
keep_prob_value = 0.6
learning_rate = 0.005
epochs = 10
cell_type='gru' # or 'lstm''


# In[50]:

# Create the graph object
graph = tf.Graph()

# to get the size of the input
x_sample = get_history(data,100,size_history).shape
y_sample = get_max_profit(data,100,nbr_sticks_for_max_profit)
# Add nodes to the graph
with graph.as_default():
    inputs_ = tf.placeholder(tf.float32, [None, None, x_sample[1]], name='inputs')
    decisions_ = tf.placeholder(tf.float32, [None, len(y_sample)], name='decisions')
    keep_prob = tf.placeholder(tf.float32, name='keep_prob')


# In[51]:

with graph.as_default():
    with tf.name_scope("RNN_Cells"):
        # Your basic LSTM cell
        # cell = tf.contrib.rnn.GRUCell(lstm_size)
        if(cell_type=='gru'):
            cell = tf.contrib.rnn.GRUCell(lstm_size)
        else:
            cell = tf.contrib.rnn.BasicLSTMCell(lstm_size)
        # Add attention to the model
        # attention = tf.contrib.rnn.AttentionCellWrapper(cell,size_history,state_is_tuple=True)
        # Add dropout to the cell
        drop = tf.contrib.rnn.DropoutWrapper(cell, output_keep_prob=keep_prob)
        # Stack up multiple LSTM layers, for deep learning
        cell = tf.contrib.rnn.MultiRNNCell([drop] * lstm_layers)
    with tf.name_scope("initial_state"):
        # Getting an initial state of all zeros
        initial_state = cell.zero_state(batch_size, tf.float32)


# In[52]:

with graph.as_default():
    with tf.name_scope("forward"):
        outputs, final_state = tf.nn.dynamic_rnn(cell, inputs_,
                                             initial_state=initial_state)


# ### Output
# 
# We only care about the final output, we'll be using that as our sentiment prediction. So we need to grab the last output with `outputs[:, -1]`, the calculate the cost from that and `decisions_`.

# In[53]:

with graph.as_default():
    with tf.name_scope("predictions"):
        logits = tf.contrib.layers.fully_connected(outputs[:, -1], len(y_sample))
        preds = tf.nn.softmax(logits, name='predictions')
        tf.summary.histogram('predictions', preds)
    with tf.name_scope("costs"):
        loss = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=decisions_, name='loss')
        cost = tf.reduce_mean(loss, name='cost')
        tf.summary.scalar('cost', cost)
    with tf.name_scope('train'):
        tvars = tf.trainable_variables()
        grad_clip = 5 # update the correct answer and few wrong answer
        grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars), grad_clip)
        optimizer = tf.train.AdamOptimizer(learning_rate).apply_gradients(zip(grads, tvars))
    merged = tf.summary.merge_all()


# ### Validation accuracy
# Here there is no known accuracy validation apart from checking the cost

# ## Training
# 
# Below is the typical training code. If you want to do this yourself, feel free to delete all this code and implement it yourself. Before you run this, make sure the `checkpoints` directory exists.

# In[54]:

get_ipython().system('mkdir -p checkpoints')


# In[55]:

test_no=1


# In[ ]:

with graph.as_default():
    saver = tf.train.Saver()

with tf.Session(graph=graph) as sess:
    test_no+=1
    log_string = 'logs/test={},cell={},epc={},lstm_s={},lstm_l={};batch_s={},kpv={},lr={}'.format(test_no, cell_type,
                epochs, lstm_size, 
                 lstm_layers, batch_size, keep_prob_value, learning_rate)
    writer = tf.summary.FileWriter(log_string,sess.graph)
    sess.run(tf.global_variables_initializer())
    iteration = 1
    for e in range(epochs):
        state = sess.run(initial_state)
        for x,y in get_batches(data,train_idx_idx, batch_size,size_history,nbr_sticks_for_max_profit):
            summary, loss, state, _ = sess.run([merged, cost, final_state, optimizer], 
                    feed_dict={inputs_: x,
                    decisions_: y,
                    keep_prob: keep_prob_value,
                    initial_state: state})
            writer.add_summary(summary, iteration)
            if iteration%10==0:
                print("Epoch: {}/{}".format(e, epochs),
                      "Iteration: {}".format(iteration),
                      "Train loss: {:.3f}".format(loss))

            if iteration%100==0:
                val_costs = []
                val_state = sess.run(cell.zero_state(batch_size, tf.float32))
                for val_x, val_y in get_batches(data,val_idx_idx, batch_size,size_history,nbr_sticks_for_max_profit):
                    feed = {inputs_: val_x,
                            decisions_: val_y,
                            keep_prob: 1,
                           initial_state: val_state}
                    val_cost, val_state = sess.run([cost,final_state], feed_dict=feed)
                    val_costs.append(val_cost)
                print("Val cost: {:.3f}".format(np.mean(val_costs)))
            iteration +=1
    saver.save(sess, "checkpoints/forex.ckpt")


# ## Testing

# In[ ]:

test_acc = []
with tf.Session(graph=graph) as sess:
    saver.restore(sess, tf.train.latest_checkpoint('checkpoints'))
    test_state = sess.run(cell.zero_state(batch_size, tf.float32))
    for ii, (x, y) in enumerate(get_batches(test_x, test_y, batch_size), 1):
        feed = {inputs_: x,
                labels_: y[:, None],
                keep_prob: 1,
                initial_state: test_state}
        batch_acc, test_state = sess.run([accuracy, final_state], feed_dict=feed)
        test_acc.append(batch_acc)
    print("Test accuracy: {:.3f}".format(np.mean(test_acc)))

