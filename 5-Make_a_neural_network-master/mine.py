# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 13:42:24 2017

@author: herimiaina
"""

from numpy import exp, array, random, dot

class NeuralNetwork():
    def __init__(self):
        random.seed(1)
         # We assign random weights to a 3 x 1 matrix, with values in the range -1 to 1
        self.synaptic_weights = 2 * random.random((3,1)) - 1
    def __sigmoid(self, x):
        return 1 / (1 + exp(-x)) 
    #http://math.stackexchange.com/questions/78575/derivative-of-sigmoid-function-sigma-x-frac11e-x                                            
    def __sigmoid_derivative(self, x):
        return x * (1 - x)
    def train(self, training_set_inputs, training_set_outputs,
              number_of_iterations):
        for iteration in range(number_of_iterations):
            output = self.think(training_set_inputs)
            error = training_set_outputs - output
            #this is the error gradient
            error_and_derivative = error * self.__sigmoid_derivative(output)
            adjustment = dot(training_set_inputs.T, error_and_derivative)
            self.synaptic_weights += adjustment
    def think(self,inputs):
        return self.__sigmoid(dot(inputs,self.synaptic_weights))                                                 
if __name__ == "__main__":
    neural_network = NeuralNetwork()
    
    print("Random weight: {0}".format(neural_network.synaptic_weights))
    training_set_inputs = array([[0, 0, 1],[1,1,1],
                                 [1,0,1],[0,1,1]])
    training_set_outputs = array([[0,1,1,0]]).T
    neural_network.train(training_set_inputs,
                         training_set_outputs,
                         10000)
    
    print("Synaptic after training: {0}".format(neural_network.synaptic_weights))
    
    print("Considering new situation [1,0,0] -> ?:")
    print(neural_network.think(array([1,0,0])))